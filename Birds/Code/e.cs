﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BirdsHunt.Code
{
    public enum GameStatus
    {
        Continue = 1,
        Pause = 2
    }

    public enum Guns
    {
        colt = 1,
        shatgun = 2,
        dorbindar = 3
    }

    public enum Birds
    {
        bird = 1,
        dragon = 2,
        hashre = 3
    }
}
