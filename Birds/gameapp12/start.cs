﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using BirdsHunt.Code;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BirdsHunt
{
    public partial class GameOptionFrm : Form
    {

        public static GameOptionFrm _Instance;
        public static GameOptionFrm Instance
        {
            get
            {
                if (_Instance==null)
                {
                    _Instance = new GameOptionFrm();
                }
                return (_Instance);
            }
        }
        public GameOptionFrm()
        {
            InitializeComponent();
        }


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
     
        private void cmbGun_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbGun.SelectedItem.ToString()== "colt")
            {
                pbGun.Image = Properties.Resources.g1;
            }
            else if (cmbGun.SelectedItem.ToString()== "shatgan")
            {
                pbGun.Image = Properties.Resources.g3;
            }
           else if (cmbGun.SelectedItem.ToString() == "dorbindar")
            {
                pbGun.Image = Properties.Resources.g2;
            }
        }
        // bird
      //  dragon
       // hashare
        private void cmbBird_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbBird.SelectedItem.ToString()== "bird")
            {
                pbBird.Image = Properties.Resources._1;
            }
         else   if (cmbBird.SelectedItem.ToString() == "dragon")
            {
                pbBird.Image = Properties.Resources.d1;
            }
          else  if (cmbBird.SelectedItem.ToString() == "hashare")
            {
                pbBird.Image = Properties.Resources.b4;
           }
        }

        private void GameOptionFrm_Load(object sender, EventArgs e)
        {
            cmbBird.SelectedIndex = 0;
            cmbGun.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Guns SelectedGun = Guns.colt;
            Birds SelectedBird = Birds.bird;

            if (cmbGun.SelectedItem.ToString() == "colt")
            {
                SelectedGun = Guns.colt;
            }
            else if (cmbGun.SelectedItem.ToString() == "shatgun")
            {
                SelectedGun = Guns.shatgun;
            }
            else if (cmbGun.SelectedItem.ToString() == "dorbindar")
            {
                SelectedGun = Guns.dorbindar;
            }

            if (cmbBird.SelectedItem.ToString() == "bird")
            {
                SelectedBird = Birds.bird;
            }
            else if (cmbBird.SelectedItem.ToString() == "dragon")
            {
                SelectedBird = Birds.dragon;
            }
            else if (cmbBird.SelectedItem.ToString() == "hashre")
            {
                SelectedBird = Birds.hashre;
            }

            GameStart playGameForm = new GameStart(SelectedGun, SelectedBird);
            this.Hide();
            playGameForm.ShowDialog();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
